var GoogleMap = function (element, options) {
    this.options = $.extend({}, GoogleMap.DEFAULTS, options);
    this.$element = $(element);
    this.map = null;

    if (this.options.height != null) {
        this.$element.css({
            height: this.options.height
        });
    }

};

GoogleMap.VERSION  = '0.1';

GoogleMap.DEFAULTS = {
    title: null,
    lat: null,
    lng: null,
    marker: null,
    scrollWheel : false,
    mapTypeControl : false,
    streetViewControl : false,
    zoom : 15,
    height: null
};

GoogleMap.prototype.make = function () {
    this.position = new google.maps.LatLng(this.options.lat, this.options.lng);

    // create new map
    this.map = new google.maps.Map(this.$element[0], {
        center: this.position,
        scrollwheel : this.options.scrollWheel,
        mapTypeControl : this.options.mapTypeControl,
        streetViewControl : this.options.streetViewControl,
        zoomControlOptions : {
            style : google.maps.ZoomControlStyle.SMALL
        },
        zoom : this.options.zoom
    });

    // support to marker in map
    new google.maps.Marker({
        position: this.position,
        map: this.map,
        title: "Locale",
        icon: this.options.marker || null
    });
};

function initialize() {

    var $element = $('[data-maps=google]'),
        data = $element.data(),
        gmap = new GoogleMap($element, data);

    console.log(gmap);

    // make the google maps
    gmap.make();

};