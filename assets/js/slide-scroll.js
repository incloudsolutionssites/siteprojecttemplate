+function ($) {
    'use strict';

    var SlideScroll = function (element, options) {
        this.options = $.extend({}, SlideScroll.DEFAULTS, options);
        this.$target = $(this.options.target);
        this.$element = $(element);
        this.$container = $(this.options.container);
        this.offsetTop = (this.$target.length ? this.$target.offset().top : this.options.offsetTop);

        var $this = this;

        this.$element.on('click', function () {
            $this.slide($this.offsetTop);
            return false;
        });

    };

    SlideScroll.VERSION  = '0.1';

    SlideScroll.DEFAULTS = {
        offsetTop: null,
        speed: 500,
        container: 'html, body',
        afterSlide: function() {}
    };

    SlideScroll.prototype.slide = function (offsetTop) {
        this.$container.animate({scrollTop: offsetTop}, this.options.speed, this.options.afterSlide);
    };

    // AFFIX PLUGIN DEFINITION
    // =======================
    function Plugin(option) {
        return this.each(function () {
            var $this = $(this)
            var data = $this.data('bs.slideScroll')
            var options = typeof option == 'object' && option

            if (!data) $this.data('bs.slideScroll', (data = new SlideScroll(this, options)))
            if (typeof option == 'string') data[option]()
        });
    };

    $.fn.slideScroll = Plugin;
    $.fn.slideScroll.Constructor = SlideScroll;

    // AFFIX DATA-API
    // ==============

    $(window).on('load', function () {

        $('[data-spy="slideScroll"]').each(function () {
            var $spy = $(this);
            var data = $spy.data();

            Plugin.call($spy, data)
        });

    });

}(jQuery);