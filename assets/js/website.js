$(function () {

    // enable dropdown hover trigger support
    $(".dropdown-hover").hover(
        function() {
            $('.dropdown-menu', this).stop( true, true );
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).stop( true, true );
            $(this).toggleClass('open');
        }
    );

});