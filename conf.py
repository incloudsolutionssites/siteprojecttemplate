import ConfigParser
import os

SETTINGS_FILE = os.path.join(os.path.dirname(__file__), '{{ project_name }}.cnf')


class AttributeDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttributeDict, self).__init__(*args, **kwargs)

        # set children with attribute dict too
        for key, value  in self.items():
            if isinstance(value, dict):
                self[key] = AttributeDict(value)

    __setattr__ = dict.__setitem__
    __getattr__ = dict.__getitem__


class Settings(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Settings, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, conf_file_path):
        self.conf_file_path = conf_file_path

    def _get_settings_parsed(self):
        if not hasattr(self, '_cached_settings'):
            self._cached_settings = ConfigParser.ConfigParser()
            self._cached_settings.read(self.conf_file_path)
        return self._cached_settings
    _settings = property(_get_settings_parsed)

    def get_section(self, section):
        return AttributeDict([
            (option, self._settings.get(section, option))
            for option in self._settings.options(section)])

    def __getattr__(self, item):
        if item in self._settings.sections():
            return self.get_section(item)
        super(Settings, self).__getattribute__(item)


settings = Settings(SETTINGS_FILE)


def get_database_settings():
    """
    Parse database settings.
    """
    return dict([(key.upper(), value) for key, value in settings.database.items()])