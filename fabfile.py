#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2015, Configr Inc. <hello@configr.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2

__version__ = (1, 0, 0, 'final')

import sys
import os
import datetime
import time
from fabric.api import *
from conf import settings


BASE_HEADER = """%(now)s
using project '%(app)s'
%(desc)s
"""


##
## change git branches
##

def pull(git_branch='master'):
    """
    Only on local, to change branches and pull
    :param git_branch:
        what branch to pull.
    """
    env.git_origin = settings.project.git_origin
    env.git_branch = git_branch


#
# avaiable enviroments
#
def localhost():
    env.app = settings.project.name
    env.app_root = os.path.dirname(__file__)
    env.virtual = os.path.join(env.app_root, '.virtualenv')
    env.pip = os.path.join(env.app_root, '.virtualenv', "bin", "pip")
    env.python = os.path.join(env.app_root, '.virtualenv', "bin", "python")


def production():
    env.app = settings.project.name
    env.host = settings.server.host
    env.user = settings.server.user
    env.hosts = ['%s@%s' % (env.user, env.host)]
    env.app_root = '/home/%s/apps_wsgi/%s' % (env.user, env.app)
    env.git_origin = settings.project.git_origin
    env.git_branch = settings.project.git_branch
    env.virtual_path = '/home/%s/.virtualenvs/%s' % (env.user, settings.project.domain)
    env.virtual = '%s/bin/activate' % (env.virtual_path)
    env.manage = env.app_root + '/manage.py'
    env.django_settings = 'src.settings.production'


##
## available commands
##

def provision(update_rep=False):
    # starts provision
    start = time.time()

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    sys.stdout.write(BASE_HEADER % {
        "now": datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"),
        "app": env.app, "desc": "Provision local developer enviroment"
    })

    with lcd(env.path):
        # Create virtualenv if not exists
        if not os.path.exists(env.virtual):
            command = "virtualenv %s" % env.virtual
            local(command)

        # allow to update repository
        if getattr(env, 'git_branch', None):
            # clone repository
            command = 'test -d %s/.git || git clone %s %s -b %s'
            local(command % (env.app_root, env.git_origin, env.app_root, env.git_branch))

            # update repository
            command = 'git checkout %s && git pull origin %s'
            local(command % (env.git_branch, env.git_branch))

        # Run install in requirements
        command = "%(pip)s install -r requirements.txt" % env
        local(command)

        # Run database migrations
        command = "%(python)s manage.py migrate" % env
        local(command)

    final = time.time()
    puts('\nexecution finished in %.2fs' % (final - start))


def deploy():
    # starts deploy
    start = time.time()

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    sys.stdout.write(BASE_HEADER % {
        "now": datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"),
        "app": env.app, "desc": "App deploy"
    })

    # clone repository
    command = 'test -d %s/.git || (rm -Rf %s; git clone %s %s -b %s)'
    run(command % (env.app_root, env.app_root, env.git_origin, env.app_root, env.git_branch))

    # apply media folder own
    command = 'test -d %s/media || mkdir %s/media'
    run(command % (env.app_root, env.app_root))

    # update repository
    command = 'cd "%s" && git reset --hard && git pull && git checkout -B %s origin/%s && git pull'
    run(command % (env.app_root, env.git_branch, env.git_branch))

    # update python package
    command = 'source %s; cd %s; pip install -r requirements.txt'
    run(command % (env.virtual, env.app_root))

    # update static media
    command = 'source %s; python %s collectstatic --noinput --settings=%s'
    run(command % (env.virtual, env.manage, env.django_settings))

    # rum migrate
    command = 'source %s; python %s migrate --noinput --settings=%s'
    run(command % (env.virtual, env.manage, env.django_settings))

    # restart uwsgi service
    # run('find %s -name "*.pyc" -delete' % env.app_root)
    # run('/etc/init.d/uwsgi reload %s' % env.domain)

    run('touch %s.wsgi' % env.app_root)

    final = time.time()
    puts('execution finished in %.2fs' % (final - start))


##
## first setup commands
##

def kinghost():
    # create ssh key
    command = (
        'test -e /home/%s/.ssh/id_rsa.pub || ('
        'ssh-keygen -t rsa;'
        'cat /home/%s/.ssh/id_rsa.pub)')
    run(command % (env.user, env.user))

    # add current machine into .ssh/authorized_keys
    command = (
        'cat ~/.ssh/id_rsa.pub | '
        'ssh %s@%s \'test -e ~/.ssh/authorized_keys || cat >> ~/.ssh/authorized_keys\'; '
        'ssh %s@%s \'chmod 600 ~/.ssh/authorized_keys\'')
    local(command % (env.user, env.host, env.user, env.host))

    # update python path
    command = 'export PYTHONPATH="/home/%s/apps_wsgi/.site-packages/"'
    run(command % (env.user,))

    # install virtualenv
    command = (
        'test -d /home/%s/apps_wsgi/.site-packages || ('
        'mkdir /home/%s/apps_wsgi/.site-packages; '
        'easy_install -a --install-dir=$PYTHONPATH virtualenv)')
    run(command % (env.user, env.user))

    # create virtualenvs folder
    command = 'test -d /home/%s/.virtualenvs/ || mkdir /home/%s/.virtualenvs/'
    run(command % (env.user, env.user))

    # create app virtualenv
    command = 'test -d %s || python /home/%s/apps_wsgi/.site-packages/virtualenv %s'
    run(command % (env.virtual_path, env.user, env.virtual_path))

    # create static folder
    command = 'test -d /home/%s/www/static/ || mkdir /home/%s/www/static/'
    run(command % (env.user, env.user))

    # update wsgi file
    local_wsgi_path = os.path.join(os.path.dirname(__file__), '%s.wsgi' % env.app)
    command = 'echo "%s" > /home/%s/apps_wsgi/%s.wsgi'
    run(command % (open(local_wsgi_path, 'r').read(), env.user, env.app))


def setup():
    # starts prepare
    start = time.time()

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    if settings.server.alias == 'kinghost':
        kinghost()

    final = time.time()
    puts('execution finished in %.2fs' % (final - start))