# coding: utf-8
"""
Django settings for {{ project_name }} project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

_ = lambda x: x

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

SITE_ID = 1

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '{{ secret_key }}'

# Application definition

INSTALLED_APPS = (
    'tinybox.admins.flat_admin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.sitemaps',

    # site apps
    'website',

    # plugins
    'tinybox.core.banners',
    'tinybox.core.company',
    'tinybox.core.config',
    'tinybox.core.contact',
    'tinybox.core.navigation',
    'tinybox.core.pages',

    # Add extra plugins below
    # extra plugins

    # robots
    'robots',
)

# Middleware is a framework of hooks into Django’s request/response processing.
# https://docs.djangoproject.com/en/1.9/topics/http/middleware/
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'tinybox.core.config.middlewares.SiteStatusMiddleware',
)

ROOT_URLCONF = 'src.urls'

WSGI_APPLICATION = 'src.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'pt-br'

LANGUAGES = (
    ('pt-BR', _('Portuguese')),
)

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.i18n',
                'django.core.context_processors.request',
                'django.core.context_processors.media',
                'django.core.context_processors.static',
            ],
        },
    },
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "assets"),
)

# A tuple that lists people who get code error notifications.
# https://docs.djangoproject.com/en/1.9/ref/settings/#admins
ADMINS = MANAGERS = (
    ('Guilherme Yano', 'guilherme@systembrasil.com.br'),
    ('Rafael Silva', 'rafaelsouza@systembrasil.com.br'),
)

# Site media files
SITE_MEDIA = {
    '__all__': {
        'css': {
            'all': [
                'css/bootstrap.min.css',
                'css/font-awesome.min.css'
            ]
        },
        'js': [
            (('src', 'js/website.min.js'), ('defer', 'defer')),
            (('src', 'js/slide-scroll.min.js'), ('defer', 'defer')),
        ]
    },
    'debug': {'css': {'all': ['css/website.css']}},
    'production': {'css': {'all': ['css/website.min.css']}}
}

# Contact plugin
CONTACT_FORM = "website.forms.contact.ContactForm"
CONTACT_ALLOW_DEPARTMENTS = False

# Analytics Support
# GOOGLE_ANALYTICS_ID = 'ANALYTICS ID'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        }
    },
    'loggers': {
        'django.request': {
            'level': 'ERROR',
            'handlers': ['mail_admins'],
            'propagate': True,
        },
    }
}

# Email configuration host
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL = 'no-reply@systembrasil.com.br'
EMAIL_HOST_PASSWORD = '@m4st3rR3ply'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'tinybox.core.backends.email.EmailBackend'

# Admin settings
ADMIN_MENU = {
    'banners': {'icon': 'picture-o', 'models': {'Banner': {'superuser': False}}},
    'robots': {'models': {'Url': {'superuser': True}, 'Rule': {'superuser': True}}},
    'tagging': {'models': {'TaggedItem': {'superuser': True}, 'Tag': {'superuser': True}}},
}