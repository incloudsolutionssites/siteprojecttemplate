# -*- coding: utf-8 -*-
"""
Django production settings for {{ project_name }} project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
import os
from conf import get_database_settings, settings as project_settings
from split_settings.tools import include


if os.environ['DJANGO_SETTINGS_MODULE'] == 'src.settings.production':
    # must bypass this block if another settings module was specified
    include("base.py", scope=locals())

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = DEBUG
ALLOWED_HOSTS = [
    'www.' + project_settings.project.domain,
    project_settings.project.domain,
    project_settings.project.alternate_domain
]

CHARSET = 'utf8'

USE_TZ = True

# use cofigr database definition
DATABASES = {
    'default': get_database_settings()
}

# use configr media settings
MEDIA_URL = '/static/media/'
MEDIA_ROOT = '/home/{{ project_name }}/www/static/media/'

# use configr static settings
STATIC_URL = '/static/'
STATIC_ROOT = '/home/{{ project_name }}/www/static/'