# -*- coding: utf-8 -*-
from django import forms
from tinybox.core.contact.forms import ContactForm as BaseContactForm


class ContactForm(BaseContactForm):
    """
    Overrides ``tinybox.core.contact.forms.ContactForm`` to enable support
    to placeholders into fields.
    """
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget = forms.TextInput(attrs={
            "class": "input-lg", "placeholder": "Nome"})

        self.fields['email'].widget = forms.TextInput(attrs={
            "class": "input-lg", "placeholder": "Email"})

        self.fields['message'].widget = forms.Textarea(attrs={
            "class": "input-lg", "placeholder": "Mensagem",
            "rows": 4})
