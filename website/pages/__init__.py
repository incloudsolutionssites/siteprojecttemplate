# ------------ Import Pages ------------ #
from website.pages.home import HomePage

# ---------- Load Error Pages ---------- #
from website.pages.errors import load_error_pages

# ---------- Maintenance Pages --------- #
from website.pages.maintenance import load_maintenance_pages


# load page errors in debug True
# >>> http://localhost:8000/403/
# >>> http://localhost:8000/404/
# >>> http://localhost:8000/500/
load_error_pages()


# load maintenance pages in debug true
# >>> http://localhost:8000/under/construction/
# >>> http://localhost:8000/under/maintenance/
load_maintenance_pages()
