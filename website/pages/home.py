# -*- coding: utf-8 -*-
from tinybox.core.pages import library as page_library
from tinybox.core.pages import Page


@page_library.register(name='home', url_regex=r'^$')
class HomePage(Page):
    title = u'Início'
    description = u'Página principal do site.'
    keywords = ['inicio', 'pagina principal', 'primeira pagina']
