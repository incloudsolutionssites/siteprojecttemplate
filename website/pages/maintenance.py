from django.conf import settings as django_settings
from tinybox.core.config.settings import UNDER_CONSTRUCTION_TEMPLATE
from tinybox.core.config.settings import UNDER_MAINTENANCE_TEMPLATE
from tinybox.core.pages import library as page_library


def load_maintenance_pages(debug=django_settings.DEBUG):
    # if not debug true, not register error pages.
    if not debug:
        return

    # ------------- CONSTRUCTION ------------- #
    page_library.create_page(
        'under-construction', 'Under Construction',
        template_name=UNDER_CONSTRUCTION_TEMPLATE, regex=r'^under/construction/',
        show_in_menu=False)

    # -------------- MAINTENANCE ------------- #
    page_library.create_page(
        'under-maintenance', 'Under Maintenance',
        template_name=UNDER_MAINTENANCE_TEMPLATE, regex=r'^under/maintenance/',
        show_in_menu=False)
