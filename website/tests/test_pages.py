from django.test import TestCase
from django.test.client import RequestFactory
from tinybox.core.pages import pages


class SimpleHttpRequestFromPageTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.pages = pages.pages.values()

    def get_page_response(self, page):
        # create a request to view test
        request = self.factory.get(page.get_absolute_url())

        # get page view
        page_view = page.view

        # make a response to view
        response = page_view(request, **{"extra_context": page.get_context_data()})

        return response

    def test_response(self):

        for page in self.pages:
            msg = "Testing page ``%s`` %s %s "

            response = self.get_page_response(page)

            # get verbose status code
            status = "OK"
            status_code = response.status_code

            if status_code == 404:
                status = "NOT FOUND"
            elif status_code == 500:
                status = "INTERNAL ERROR"
            elif not status_code == 200:
                status = "ERROR %s" % status_code

            print msg % (
                page.name,
                "." * (30 - len(page.name)),
                status)

            self.assertEqual(response.status_code, 200)
