from django.conf.urls import url, include

urlpatterns = [
    # add extra urls
    # ...

    url(r'^contato/',    include('tinybox.core.contact.urls', namespace='contact')),
    url(r'^robots\.txt', include('robots.urls')),
]
